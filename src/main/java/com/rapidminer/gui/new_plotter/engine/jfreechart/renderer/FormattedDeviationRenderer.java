/**
 * Copyright (C) 2001-2018 by RapidMiner and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * http://rapidminer.com
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.rapidminer.gui.new_plotter.engine.jfreechart.renderer;

import com.rapidminer.gui.new_plotter.engine.jfreechart.RenderFormatDelegate;
import com.rapidminer.gui.new_plotter.utility.DataStructureUtils;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Shape;

import org.jfree.chart.renderer.xy.DeviationRenderer;


/**
 * @author Marius Helf
 */
public class FormattedDeviationRenderer extends DeviationRenderer implements FormattedRenderer {

	private static final long serialVersionUID = 1L;

	private final RenderFormatDelegate formatDelegate = new RenderFormatDelegate();

	public FormattedDeviationRenderer() {}

	public FormattedDeviationRenderer(boolean lines, boolean shapes) {
		super(lines, shapes);
	}

	@Override
	public RenderFormatDelegate getFormatDelegate() {
		return formatDelegate;
	}

	@Override
	public Paint getItemPaint(int seriesIdx, int valueIdx) {
		Paint paintFromDelegate = getFormatDelegate().getItemPaint(seriesIdx, valueIdx);
		return paintFromDelegate == null ? super.getItemPaint(seriesIdx, valueIdx) : paintFromDelegate;
	}

	@Override
	public Shape getItemShape(int seriesIdx, int valueIdx) {
		Shape shapeFromDelegate = getFormatDelegate().getItemShape(seriesIdx, valueIdx);
		return shapeFromDelegate == null ? super.getItemShape(seriesIdx, valueIdx) : shapeFromDelegate;
	}

	@Override
	public Paint getSeriesFillPaint(int seriesIdx) {
		Paint seriesFillPaint = getFormatDelegate().getSeriesFillPaint(seriesIdx);
		if (seriesFillPaint == null) {
			seriesFillPaint = super.getSeriesFillPaint(seriesIdx);
		}
		return seriesFillPaint;
	}

	@Override
	public Paint getItemOutlinePaint(int seriesIdx, int valueIdx) {
		return getFormatDelegate().isItemSelected(seriesIdx, valueIdx) ? super.getItemOutlinePaint(seriesIdx, valueIdx) : DataStructureUtils.setColorAlpha(Color.LIGHT_GRAY, 20);
	}
}
