/**
 * Copyright (C) 2001-2018 by RapidMiner and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * http://rapidminer.com
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.rapidminer.repository;

/**
 * Contains error codes for exceptions etc.
 * 
 * @author Simon Fischer
 * 
 */
public interface RepositoryConstants {

	int OK = 0;

	int GENERAL_ERROR = -1;

	int NO_SUCH_ENTRY = -2;

	int NO_SUCH_REVISION = -3;

	int DUPLICATE_ENTRY = -4;

	int WRONG_TYPE = -5;

	int ILLEGAL_CHARACTER = -6;

	int ILLEGAL_TYPE = -7;

	int ILLEGAL_DATA_FORMAT = -8;

	int MALFORMED_CRON_EXPRESSION = -9;

	int NO_SUCH_TRIGGER = -10;

	int MALFORMED_PROCESS = -11;

	int ACCESS_DENIED = -12;

	int NO_SUCH_GROUP = -13;

	int DUPLICATE_USERNAME = -14;

	int DUPLICATE_GROUPNAME = -15;

	int NO_SUCH_USER_IN_GROUP = -16;

	int FORBIDDEN = -17;

	int NO_SUCH_PROCESS = -18;

	int MISSING_PARAMETER = -19;

	int VERSION_OUT_OF_DATE = -20;

	int NO_SUCH_USER = -21;

	int PROCESS_FAILED = -22;

	int NO_SUCH_QUEUE = -23;

}
