/**
 * Copyright (C) 2001-2018 by RapidMiner and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * http://rapidminer.com
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.rapidminer.gui.new_plotter.configuration;

import com.rapidminer.datatable.DataTable;
import com.rapidminer.gui.new_plotter.PlotConfigurationError;
import com.rapidminer.gui.new_plotter.configuration.DataTableColumn.ValueType;
import com.rapidminer.gui.new_plotter.configuration.ValueGrouping.GroupingType;
import com.rapidminer.gui.new_plotter.listener.DimensionConfigListener;
import com.rapidminer.gui.new_plotter.utility.NumericalValueRange;
import com.rapidminer.gui.new_plotter.utility.ValueRange;
import com.rapidminer.tools.I18N;

import java.text.DateFormat;
import java.util.List;
import java.util.Set;
import java.util.Vector;


/**
 * Defines where a dimension gets its values from. Could be: the values of an attribute, all
 * attributes, etc.
 * 
 * Also defines the sort order and the value range.
 * 
 * @author Marius Helf, Nils Woehler
 * 
 */
public interface DimensionConfig extends Cloneable {

	String DEFAULT_DATE_FORMAT_STRING = "dd.MM.yyyy HH:mm";
	String DEFAULT_AXIS_LABEL = "";
	boolean DEFAULT_USE_USER_DEFINED_DATE_FORMAT = false;
	double DEFAULT_USER_DEFINED_LOWER_BOUND = 0;
	double DEFAULT_USER_DEFINED_UPPER_BOUND = 1;

	enum PlotDimension {
		VALUE(null, null), DOMAIN(I18N.getGUILabel("plotter.configuration_dialog.plot_dimension.domain.label"), I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.domain.short_label")), COLOR(I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.color.label"), I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.color.short_label")), SHAPE(I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.shape.label"), I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.shape.short_label")), SIZE(I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.size.label"), I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.size.short_label")), SELECTED(I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.selected.label"), I18N
				.getGUILabel("plotter.configuration_dialog.plot_dimension.selected.short_label"));

		// LINESTYLE

		private final String name;
		private final String shortName;

		PlotDimension(String name, String shortName) {
			this.name = name;
			this.shortName = shortName;
		}

		/**
		 * @return The display name of the enum value.
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return A shorter display name of the enum value.
		 */
		public String getShortName() {
			return shortName;
		}
	}

	Double getUserDefinedUpperBound();

	Double getUserDefinedLowerBound();

	PlotDimension getDimension();

	/**
	 * Returns the {@link DataTableColumn} from which this DimensionConfig gets its raw values.
	 */
    DataTableColumn getDataTableColumn();

	ValueGrouping getGrouping();

	/**
	 * Returns the range of data which is used to create the diagram. Note that this is not
	 * necessarily the data the user sees, because he might apply further filtering by zooming.
	 * 
	 * Might return null, which indicates that all values should be used.
	 * 
	 * Returns a clone of the actual range, so changing the returned object does not actually change
	 * the range of this {@link DimensionConfig}.
	 */
    ValueRange getUserDefinedRangeClone(DataTable dataTable);

	/**
	 * Returns the label of the dimension config that will be shown in the GUI.
	 */
    String getLabel();

	List<PlotConfigurationError> getErrors();

	List<PlotConfigurationError> getWarnings();

	Vector<GroupingType> getValidGroupingTypes();

	ValueType getValueType();

	Set<DataTableColumn.ValueType> getSupportedValueTypes();

	int getId();

	boolean isValid();

	boolean isAutoRangeRequired();

	boolean isLogarithmic();

	boolean isAutoNaming();

	boolean isGrouping();

	boolean isNominal();

	boolean isNumerical();

	boolean isDate();

	boolean isUsingUserDefinedLowerBound();

	boolean isUsingUserDefinedUpperBound();

	void setGrouping(ValueGrouping grouping);

	// public void setSortProvider(SortProvider sortProvider);
    void setDataTableColumn(DataTableColumn column);

	void setUserDefinedRange(NumericalValueRange range);

	void setLogarithmic(boolean logarithmic);

	void setAutoNaming(boolean autoNaming);

	void setLabel(String label);

	void setUpperBound(Double upperBound);

	void setLowerBound(Double lowerBound);

	void setUseUserDefinedUpperBound(boolean useUpperBound);

	void setUseUserDefinedLowerBound(boolean useLowerBound);

	void removeDimensionConfigListener(DimensionConfigListener l);

	void addDimensionConfigListener(DimensionConfigListener l);

	void colorSchemeChanged();

	/**
	 * Returns a {@link DateFormat} to be used for formatting dates on this axis.
	 * 
	 * @return the date format used to format dates on this axis.
	 */
    DateFormat getDateFormat();

	void setUserDefinedDateFormatString(String formatString);

	String getUserDefinedDateFormatString();

	void setUseUserDefinedDateFormat(boolean yes);

	boolean isUsingUserDefinedDateFormat();
}
