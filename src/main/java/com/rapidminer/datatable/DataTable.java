/**
 * Copyright (C) 2001-2018 by RapidMiner and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * http://rapidminer.com
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.rapidminer.datatable;

import com.rapidminer.Process;
import com.rapidminer.gui.plotter.charts.AbstractChartPanel.Selection;
import com.rapidminer.report.Tableable;

import java.io.PrintWriter;
import java.util.Iterator;


/**
 * A data table that contains Object arrays that record process results, data, etc. Instances of
 * this class are automatically created by {@link Process#getDataTables()} and are used mainly by
 * the {@link com.rapidminer.operator.visualization.ProcessLogOperator}. On the other hand, also
 * {@link com.rapidminer.example.ExampleSet}s can also be used as an data table object.
 * 
 * @author Ingo Mierswa
 */
public interface DataTable extends Iterable<DataTableRow>, Tableable {

	/**
	 * Indicates if the column with the given index is nominal. For numerical or date columns, the
	 * value false should be returned.
	 */
    boolean isNominal(int index);

	/**
	 * Indicates if the column with the given index is nominal. For numerical or date columns, the
	 * value false should be returned.
	 */
    boolean isTime(int index);

	/**
	 * Indicates if the column with the given index is nominal. For numerical or date columns, the
	 * value false should be returned.
	 */
    boolean isDate(int index);

	/**
	 * Indicates if the column with the given index is nominal. For numerical or date columns, the
	 * value false should be returned.
	 */
    boolean isDateTime(int index);

	/**
	 * Indicates if the column with the given index is nominal. For numerical or date columns, the
	 * value false should be returned.
	 */
    boolean isNumerical(int index);

	/**
	 * If a column is nominal, the index value must be mapped to the nominal value by this method.
	 * If the given column is not nominal, this method might throw a {@link NullPointerException}.
	 */
    String mapIndex(int column, int index);

	/**
	 * If a column is nominal, the nominal value must be mapped to a (new) index by this method. If
	 * the given column is not nominal, this method might throw a {@link NullPointerException}.
	 */
    int mapString(int column, String value);

	/** Returns the name of the i-th column. */
	@Override
    String getColumnName(int i);

	/** Returns the column index of the column with the given name. */
    int getColumnIndex(String name);

	/** Returns the weight of the column or Double.NaN if no weight is available. */
    double getColumnWeight(int i);

	/** Returns true if this data table is supporting column weights. */
    boolean isSupportingColumnWeights();

	/** Returns the total number of columns. */
    int getNumberOfColumns();

	/**
	 * Returns the total number of special columns. Please note that these columns do not need to be
	 * in an ordered sequence. In order to make sure that a column is a special column the method
	 * {@link #isSpecial(int)} should be used.
	 */
    int getNumberOfSpecialColumns();

	/**
	 * Returns true if this column is a special column which might usually not be used for some
	 * plotters, for example weights or labels.
	 */
    boolean isSpecial(int column);

	/** Returns an array of all column names. */
    String[] getColumnNames();

	/** Returns the name of this data table. */
    String getName();

	/** Sets the name of the data table. */
    void setName(String name);

	/** Adds the given {@link DataTableRow} to the table. */
    void add(DataTableRow row);

	/** Returns an iterator over all {@link DataTableRow}s. */
	@Override
    Iterator<DataTableRow> iterator();

	/**
	 * Returns the data table row with the given index. Please note that this method is not
	 * guaranteed to be efficiently implemented. If you want to scan the complete data table you
	 * should use the iterator() method instead.
	 */
    DataTableRow getRow(int index);

	/** Returns the total number of rows. */
    int getNumberOfRows();

	/**
	 * Returns the number of different values for the i-th column. Might return -1 or throw an
	 * exception if the specific column is not nominal.
	 */
    int getNumberOfValues(int column);

	/** Must deliver the proper value as string, i.e. the mapped value for nominal columns. */
    String getValueAsString(DataTableRow row, int column);

	/** Adds a table listener listening for data changes. */
    void addDataTableListener(DataTableListener dataTableListener);

	/**
	 * Adds a table listener listening for data changes.
	 * 
	 * @param weakReference
	 *            if true, the listener is stored in a weak reference, so that the listener
	 *            mechanism does not keep garbage collection from deleting the listener.
	 */
    void addDataTableListener(DataTableListener dataTableListener, boolean weakReference);

	/** Removes the given listener from the list of data change listeners. */
    void removeDataTableListener(DataTableListener dataTableListener);

	/** Writes the table into the given writer. */
    void write(PrintWriter out);

	/** Performs a sampling of this data table. Following operations should only work on the sample. */
    DataTable sample(int newSize);

	/** Returns true if this data table contains missing values. */
    boolean containsMissingValues();

	// public boolean isDeselected(int index);
    boolean isDeselected(String id);

	void setSelection(Selection selection);

	int getSelectionCount();
}
